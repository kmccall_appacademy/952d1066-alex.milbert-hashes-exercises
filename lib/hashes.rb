# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hash_length = Hash.new(0)
  str.split.each do |word|
    hash_length[word] = word.length
  end
  hash_length
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  v_lues = hash.values.sort
  hash.each do |k,v|
    if hash[k] == v_lues[-1]
      return k
    end
  end
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  old_keys = older.keys
  new_keys = newer.keys
  older.each do |k_o,v_o|
    if new_keys.include?(k_o)
      older[k_o] = newer[k_o]
    end
  end
  newer.each do |k_n,v_n|
    if old_keys.include?(k_n)
      next
    else
      older[k_n] = v_n
    end
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  count_hash = Hash.new(0)
  letters = word.split('')
  letters.each do |el|
    count_hash[el] += 1
  end
  p count_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hash = Hash.new(0)
  arr.each do |el|
    if uniq_hash.has_key?(el) == true
      next
    elsif uniq_hash.has_key?(el) == false
      uniq_hash[el] = 0
    end
  end
  p uniq_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_hash = Hash.new(0)
  numbers.each do |num|
    if num.even?
      even_odd_hash[:even] += 1
    else
      even_odd_hash[:odd] += 1
    end
  end
  p even_odd_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = 'aeiou'
  vowel_hash = Hash.new(0)
  vowel_split = string.split('')
  vowel_split.each do |el|
    if vowels.include?(el)
      vowel_hash[el] += 1
    end
  end
  p x = vowel_hash.sort_by {|k,v| v}
  p z = vowel_hash.values.sort
  # p y = x.to_h
  x.each_index do |idx|
    if x[idx][1] == z[-1]
      return p x[idx][0]

    end
  end
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
end

def character_count(str)
end
